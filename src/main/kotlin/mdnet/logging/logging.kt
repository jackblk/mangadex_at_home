/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.logging

import org.slf4j.Logger

inline fun Logger.error(msg: () -> String) {
    if (this.isErrorEnabled) {
        this.error(msg())
    }
}
inline fun Logger.warn(msg: () -> String) {
    if (this.isWarnEnabled) {
        this.warn(msg())
    }
}
inline fun Logger.info(msg: () -> String) {
    if (this.isInfoEnabled) {
        this.info(msg())
    }
}
inline fun Logger.debug(msg: () -> String) {
    if (this.isDebugEnabled) {
        this.debug(msg())
    }
}
inline fun Logger.trace(msg: () -> String) {
    if (this.isTraceEnabled) {
        this.trace(msg())
    }
}
inline fun Logger.error(e: Throwable, msg: () -> String) {
    if (this.isErrorEnabled) {
        this.error(msg(), e)
    }
}
inline fun Logger.warn(e: Throwable, msg: () -> String) {
    if (this.isWarnEnabled) {
        this.warn(msg(), e)
    }
}
inline fun Logger.info(e: Throwable, msg: () -> String) {
    if (this.isInfoEnabled) {
        this.info(msg(), e)
    }
}
inline fun Logger.debug(e: Throwable, msg: () -> String) {
    if (this.isDebugEnabled) {
        this.debug(msg(), e)
    }
}
inline fun Logger.trace(e: Throwable, msg: () -> String) {
    if (this.isTraceEnabled) {
        this.trace(msg(), e)
    }
}
