/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
*/
package mdnet.metrics

import org.http4k.core.HttpTransaction
import org.http4k.filter.HttpTransactionLabeler

class PostTransactionLabeler : HttpTransactionLabeler {
    override fun invoke(transaction: HttpTransaction): HttpTransaction {
        return transaction.copy(
            labels = mapOf(
                "method" to transaction.request.method.toString(),
                "status" to transaction.response.status.code.toString(),
                "path" to transaction.routingGroup,
                "cache" to (transaction.response.header("X-Cache") ?: "MISS").uppercase()
            )
        )
    }
}
